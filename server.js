// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
var cookie = require('cookie');
var crypto = require('crypto');
var sha256 = crypto.createHash('sha256');

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

app.get("/sessions", function(req, res, next) {
	db.all('SELECT ident, token FROM sessions;', function(err, data) {
		res.json(data);
	});
});

app.post("/login", function(req, res, next) {
	db.all('SELECT ident FROM users WHERE ident = ? AND password = ?;',[req.body.login, req.body.password], function(err, data){
		if(Object.keys(data).length != 0){
			sha256.update(Math.random().toString(36).substr(2) + req.body.password, "utf8");
			var result = sha256.digest("hex");
			
			var ins = db.prepare('INSERT INTO sessions (ident, token) VALUES(?,?)');
			ins.run(req.body.login, result);
			ins.finalize();
			
			res.setHeader('Set-Cookie', cookie.serialize('name', result, {
				httpOnly: true,
				maxAge: 60 * 60 * 24 * 7 // 1 week 
			}));
			
			res.json("{status : true, token : " + result + "}");
		}else{
			res.json("{status : false}");
		}
	});
});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
